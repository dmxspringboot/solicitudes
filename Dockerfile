FROM openjdk:17
ENV JAVA_OPTIONS -server -Xms300m -Xmx512m
ENV URL_DB=URL_DATABASE \
	USER_DB=USER_DATABASE \
	PASS_DB=PASS_DATABASE
RUN echo $URL_DB
COPY target/solicitudes-*.jar /solicitudes-*.jar
EXPOSE 8080/tcp
ENTRYPOINT exec java $JAVA_OPTIONS -jar /solicitudes-*.jar