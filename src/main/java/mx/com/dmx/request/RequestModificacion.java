package mx.com.dmx.request;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import mx.com.dmx.dto.MotivoDTO;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RequestModificacion {
	
	private String idSolicitud;
	private String idEstatus;
	private List<MotivoDTO> motivo;
	private String fechaCambio;

}
