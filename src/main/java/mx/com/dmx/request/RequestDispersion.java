package mx.com.dmx.request;

import java.math.BigDecimal;

import lombok.Data;

@Data
public class RequestDispersion {
	
	private String idSolicitud;
	private Integer idCredito;
	private BigDecimal capitalDispersado;
	private BigDecimal monto;
	private BigDecimal tasa;
	private Integer plazo;
	private String frecuencia;

}
