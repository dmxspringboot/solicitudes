package mx.com.dmx.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import mx.com.dmx.dto.ClienteDTO;
import mx.com.dmx.dto.SolicitudDTO;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RequestSolicitud {
	
	private String promotor;
	private String empresa;
	private ClienteDTO cliente;
	private SolicitudDTO solicitud;

}
