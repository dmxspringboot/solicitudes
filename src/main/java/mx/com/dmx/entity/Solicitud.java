package mx.com.dmx.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.OneToMany;
import jakarta.persistence.OneToOne;
import jakarta.persistence.Table;
import jakarta.persistence.Temporal;
import jakarta.persistence.TemporalType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name="solicitud")
public class Solicitud implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@Column(name="ID_SOLICITUD")
	private String idSolicitud;
	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name = "ID_CLIENTE", referencedColumnName = "ID_CLIENTE", nullable = false)
	private Cliente cliente;
	@Column(name="MONTO")
	private BigDecimal monto;
	@Column(name="PRODUCTO", nullable = false)
	private String producto;
	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name = "ID_TIPO_SOLICITUD", referencedColumnName = "ID_TIPO_SOLICITUD", nullable = false)
	private TipoSolicitud tipoSolicitud;
	@Column(name="TASA", nullable = false)
	private BigDecimal tasa;
	@Column(name="PLAZO", nullable = false)
	private Integer plazo;
	@Column(name="FRECUENCIA", nullable = false)
	private String frecuencia;
	@Column(name="FECHA_SOLICITUD", nullable = false)
	@Temporal(TemporalType.DATE)
	private Date fechaSolicitud;
	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name = "ID_PROMOTOR", referencedColumnName = "ID_PROMOTOR")
	private Promotor promotor;
	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name = "ID_ESTATUS", referencedColumnName = "ID_ESTATUS")
	private Estatus estatus;
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name = "ID_SOLICITUD", referencedColumnName = "ID_SOLICITUD")
	private Set<Motivo> motivos = new HashSet<>();
	@Column(name="FECHA_MODIFICACION")
	@Temporal(TemporalType.DATE)
	private Date fechaModificacion;
	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name = "ID_CREDITO", referencedColumnName = "ID_CREDITO")
	private Credito credito;

}
