package mx.com.dmx.entity;

import java.math.BigDecimal;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name="credito")
public class Credito {
	
	@Id
	@Column(name="ID_CREDITO")
	private Integer idCredito;
	@Column(name="CAPITAL_DISPERSADO", nullable = false)
	private BigDecimal capitalDispersado;
	@Column(name="MONTO", nullable = false)
	private BigDecimal monto;
	@Column(name="TASA", nullable = false)
	private BigDecimal tasa;
	@Column(name="PLAZO", nullable = false)
	private Integer plazo;
	@Column(name="FRECUENCIA", nullable = false)
	private String frecuencia;

}
