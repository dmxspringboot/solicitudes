package mx.com.dmx.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name="motivo")
public class Motivo {
	
	@Id
	@Column(name = "ID_MOTIVO")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer idMotivo;
	@Column(name = "ID_SOLICITUD", nullable = false)
	private String idSolicitud;
	@Column(name = "CODIGO", nullable = false)
	private String codigo;
	@Column(name = "DESCRIPCION", nullable = false)
	private String descripcion;

}
