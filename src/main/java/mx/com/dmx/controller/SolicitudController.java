package mx.com.dmx.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import mx.com.dmx.entity.Cliente;
import mx.com.dmx.entity.Solicitud;
import mx.com.dmx.request.RequestDispersion;
import mx.com.dmx.request.RequestModificacion;
import mx.com.dmx.request.RequestSolicitud;
import mx.com.dmx.service.ClienteService;
import mx.com.dmx.service.SolicitudService;

@RestController
@RequestMapping("solicitud")
public class SolicitudController {
	
	@Autowired
	private SolicitudService solicitudService;
	
	@Autowired
	private ClienteService clienteService;
	
	@PostMapping(value = "/alta")
	public ResponseEntity<String> alta(@RequestBody final RequestSolicitud requestSolicitud) {
		ResponseEntity<String> response;
		try {
			solicitudService.alta(requestSolicitud);
			response = new ResponseEntity<>("Se ingreso una nueva solicitud al promotor: " + requestSolicitud.getPromotor(), HttpStatus.CREATED); 
		} catch(Exception e) {
			e.printStackTrace();
			response = new ResponseEntity<>("Ocurrio un error al asignar la solicitud al promotor: " + requestSolicitud.getPromotor(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return response;
	}
	
	@PostMapping(value = "/actualiza")
	public ResponseEntity<String> modifica(@RequestBody final RequestModificacion requestModificacion) {
		ResponseEntity<String> response; 
		try {
			Solicitud solicitud = solicitudService.consulta(requestModificacion.getIdSolicitud());
			if (solicitud == null) {
				response = new ResponseEntity<String>("No se encontraron datos de la solicud.", HttpStatus.NOT_FOUND);
			} else if (solicitud.getEstatus() != null && 
							"ERROR".equalsIgnoreCase(solicitud.getEstatus().getIdEstatus())) {
				Cliente cliente = clienteService.consulta(solicitud.getCliente().getIdCliente());
				if (cliente == null) {
					response = new ResponseEntity<String>("No se encontraron datos del cliente de la solicitud.", HttpStatus.NOT_FOUND);
				} else {
					solicitudService.actualiza(requestModificacion);
					response = new ResponseEntity<String>("Se realizaron cambios en los datos de la solicud " + requestModificacion.getIdSolicitud(), HttpStatus.OK);
				}
			} else {
				solicitudService.actualiza(requestModificacion);
				response = new ResponseEntity<String>("Se realizaron cambios en los datos de la solicud " + requestModificacion.getIdSolicitud(), HttpStatus.OK);
			}
		} catch(Exception e) {
			e.printStackTrace();
			response = new ResponseEntity<>("Ocurio un error al intentar realizar cambios en la solicitud.", HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return response;
	}
	
	@PostMapping(value = "/dispersion")
	public ResponseEntity<String> dispersion(@RequestBody final RequestDispersion requestDispersion) {
		ResponseEntity<String> response;
		try {
			Solicitud solicitud = solicitudService.consulta(requestDispersion.getIdSolicitud());
			if (solicitud == null) {
				response = new ResponseEntity<String>("No se encontraron datos de la solicud.", HttpStatus.NOT_FOUND);
			} else if (solicitud.getEstatus() != null && 
							"ERROR".equalsIgnoreCase(solicitud.getEstatus().getIdEstatus())) {
				Cliente cliente = clienteService.consulta(solicitud.getCliente().getIdCliente());
				if (cliente == null) {
					response = new ResponseEntity<String>("No se encontraron datos del cliente de la solicitud.", HttpStatus.NOT_FOUND);
				} else {
					solicitudService.dispersion(requestDispersion);
					response = new ResponseEntity<String>("Se realizo la dispersión del crédito" + requestDispersion.getIdCredito() 
															+ " de la solicud " + requestDispersion.getIdSolicitud(), HttpStatus.OK);
				}
			} else {
				solicitudService.dispersion(requestDispersion);
				response = new ResponseEntity<String>("Se realizo la dispersión del crédito" + requestDispersion.getIdCredito() 
														+ " de la solicud " + requestDispersion.getIdSolicitud(), HttpStatus.OK);
			}
		} catch(Exception e) {
			e.printStackTrace();
			response = new ResponseEntity<>("Ocurio un error al intentar realizar la dispersión del crédito " 
											+ requestDispersion.getIdCredito() + " de la solicitud " 
											+ requestDispersion.getIdSolicitud() , HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return response;
	}

}
