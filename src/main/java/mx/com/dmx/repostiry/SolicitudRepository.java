package mx.com.dmx.repostiry;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import mx.com.dmx.entity.Solicitud;

@Repository
public interface SolicitudRepository extends JpaRepository<Solicitud, String>  {

}
