package mx.com.dmx.repostiry;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import mx.com.dmx.entity.Cliente;

@Repository
public interface ClienteRepository extends JpaRepository<Cliente, Long>  {

}
