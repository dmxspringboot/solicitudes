package mx.com.dmx.dto;

import lombok.Data;

@Data
public class ClienteDTO {
	
	private String nombre;
	private String apellidoPaterno;
	private String apellidoMaterno;

}
