package mx.com.dmx.dto;

import lombok.Data;

@Data
public class MotivoDTO {
	
	private String codigo;
	private String descripcion;

}
