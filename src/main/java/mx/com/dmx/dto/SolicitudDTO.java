package mx.com.dmx.dto;

import java.math.BigDecimal;

import lombok.Data;

@Data
public class SolicitudDTO {
	
	private String idSolicitud;
	private BigDecimal monto;
	private String producto;
	private String tipoSolicitudStr;
	private String idTipoSolicitud;
	private BigDecimal tasa;
	private Integer plazo;
	private String frecuencia;
	private String fechaSolicitud;

}
