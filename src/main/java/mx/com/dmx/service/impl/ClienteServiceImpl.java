package mx.com.dmx.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import mx.com.dmx.entity.Cliente;
import mx.com.dmx.repostiry.ClienteRepository;
import mx.com.dmx.service.ClienteService;

@Service
public class ClienteServiceImpl implements ClienteService {
	
	@Autowired
	private ClienteRepository repository;

	@Override
	public Cliente consulta(Long id) {
		return repository.findById(id).orElse(null);
	}

}
