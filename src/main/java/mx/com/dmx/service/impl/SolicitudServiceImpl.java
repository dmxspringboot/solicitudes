package mx.com.dmx.service.impl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;
import mx.com.dmx.dto.MotivoDTO;
import mx.com.dmx.entity.Cliente;
import mx.com.dmx.entity.Credito;
import mx.com.dmx.entity.Estatus;
import mx.com.dmx.entity.Motivo;
import mx.com.dmx.entity.Promotor;
import mx.com.dmx.entity.Solicitud;
import mx.com.dmx.entity.TipoSolicitud;
import mx.com.dmx.repostiry.SolicitudRepository;
import mx.com.dmx.request.RequestDispersion;
import mx.com.dmx.request.RequestModificacion;
import mx.com.dmx.request.RequestSolicitud;
import mx.com.dmx.service.SolicitudService;

@Service
@Slf4j
public class SolicitudServiceImpl implements SolicitudService {
	
	@Autowired
	private SolicitudRepository repository;

	@Override
	public void alta(RequestSolicitud request) throws Exception {
		Date fechaSolicitud = getFechaDate(request.getSolicitud().getFechaSolicitud());
		TipoSolicitud tipoSolicitud = new TipoSolicitud();
		tipoSolicitud.setIdTipoSolicitud(request.getSolicitud().getIdTipoSolicitud());
		tipoSolicitud.setTipoSolicitud(request.getSolicitud().getTipoSolicitudStr());
		Promotor promotor = Promotor.builder()
				.idPromotor(request.getPromotor())
				.build();
		Cliente cliente = Cliente.builder()
				.nombre(request.getCliente().getNombre())
				.apellidoPaterno(request.getCliente().getApellidoPaterno())
				.apellidoMaterno(request.getCliente().getApellidoMaterno())
				.build();
		Solicitud solicitud = Solicitud.builder()
				.idSolicitud(request.getSolicitud().getIdSolicitud())
				.monto(request.getSolicitud().getMonto())
				.producto(request.getSolicitud().getProducto())
				.tipoSolicitud(tipoSolicitud)
				.tasa(request.getSolicitud().getTasa())
				.plazo(request.getSolicitud().getPlazo())
				.frecuencia(request.getSolicitud().getFrecuencia())
				.fechaSolicitud(fechaSolicitud)
				.promotor(promotor)
				.cliente(cliente)
				.build();
		log.info("Solicitud: " + solicitud);
		repository.save(solicitud);
	}

	@Override
	public Solicitud consulta(String id) {
		return repository.findById(id).orElse(null);
	}

	@Override
	public void actualiza(RequestModificacion request) throws Exception {
		Solicitud solicitud = repository.findById(request.getIdSolicitud()).orElse(null);
		log.info("Solicitud: " + solicitud);
		Date fechaCambio = getFechaDate(request.getFechaCambio());
		Set<Motivo> motivo = new HashSet<>();
		for (MotivoDTO motivoDto : request.getMotivo()) {
			Motivo motivoEntity = Motivo.builder()
					.idSolicitud(solicitud.getIdSolicitud())
					.codigo(motivoDto.getCodigo())
					.descripcion(motivoDto.getDescripcion())
					.build();
			motivo.add(motivoEntity);
		}
		Estatus estatus = Estatus.builder()
				.idEstatus(request.getIdEstatus())
				.build();
		solicitud.setEstatus(estatus);
		solicitud.setMotivos(motivo);
		solicitud.setFechaModificacion(fechaCambio);
		repository.save(solicitud);
	}

	@Override
	public void dispersion(RequestDispersion request) throws Exception {
		Solicitud solicitud = repository.findById(request.getIdSolicitud()).orElse(null);
		log.info("Solicitud: " + solicitud);
		Date fechaCambio = new Date();
		Credito credito = Credito.builder()
				.capitalDispersado(request.getCapitalDispersado())
				.frecuencia(request.getFrecuencia())
				.idCredito(request.getIdCredito())
				.monto(request.getMonto())
				.plazo(request.getPlazo())
				.tasa(request.getTasa())
				.build();
		solicitud.setFechaModificacion(fechaCambio);
		solicitud.setCredito(credito);
		repository.save(solicitud);
		
	}
	
	private Date getFechaDate(String fecha) throws ParseException {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
		return sdf.parse(fecha);
	}

}
