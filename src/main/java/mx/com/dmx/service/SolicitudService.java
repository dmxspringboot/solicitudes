package mx.com.dmx.service;

import mx.com.dmx.entity.Solicitud;
import mx.com.dmx.request.RequestDispersion;
import mx.com.dmx.request.RequestModificacion;
import mx.com.dmx.request.RequestSolicitud;

public interface SolicitudService {
	
	void alta(final RequestSolicitud request) throws Exception;
	
	Solicitud consulta(final String id);
	
	void actualiza(final RequestModificacion request) throws Exception;
	
	void dispersion(final RequestDispersion request) throws Exception;

}
