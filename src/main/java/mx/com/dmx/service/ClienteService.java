package mx.com.dmx.service;

import mx.com.dmx.entity.Cliente;

public interface ClienteService {
	
	Cliente consulta(Long id);

}
